import { QueueConfiguration, QueuetFactory, QueuetMessageQueue, QueueType } from "queuet";
import { env } from "../utils/wrappers/env-wrapper";
import { UserRepository } from "../repositories/user.repository";

export class _QueuetService {

    private readonly mq: QueuetMessageQueue;

    public constructor() {

        const queueConfig: QueueConfiguration = {
            vendor: (env.mq.vendor as QueueType),
            queueUrl: env.mq.url
        }

        switch(queueConfig.vendor) {
            case QueueType.AmazonSQS:             
                queueConfig.aws = {
                    region: env.mq.awsRegion
                }
                break;
            case QueueType.RabbitMQ:
                queueConfig.rabbitMq = {
                    queueName: env.mq.rabbitName
                }                
        }

        this.mq = QueuetFactory.getInstance(queueConfig);
    }

    public async registerEvents(): Promise<void> {
        setInterval(this.readMessages.bind(this), env.mq.readInterval);
    }

    private async readMessages(): Promise<void> {

        try {

            const messages = await this.mq.receiveMessage();
            if (messages.length === 0) return;
            
            for (const message of messages) {
                
                try {

                    const events: any[] = JSON.parse(message.messageContent);

                    for(const event of events) {
                        await UserRepository.findOne({where: {userId: event.id}});              
                    }

                } catch (error) {
                        console.log(error)
                }
                
            }

            await this.mq.deleteMessage(...messages);

        } catch (error) {
            console.log(error);
        }

    }

    public async sendMessage() {
        this.mq.sendMessageImmediately(JSON.stringify({test: "test"}));
    }

}

export const qs = new _QueuetService();
