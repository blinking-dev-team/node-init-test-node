import { Entity, PrimaryColumn, Column } from "typeorm";
import { UserStatus } from "../models/enums/user-status.enum";

@Entity()
export class User {

    @PrimaryColumn()
    userId: number;

    @Column({nullable: true, type: 'timestamp with time zone'})
    dateOfBirth: Date;

    @Column({nullable: true, type: 'character varying'})
    name: string;
    
    @Column({nullable: true, type: 'enum', enum: UserStatus})
    userStatus: UserStatus;

}