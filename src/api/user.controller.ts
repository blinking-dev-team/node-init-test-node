import { sendResponse } from "../utils/wrappers/response-wrapper";
import { UserRepository } from "../repositories/user.repository";
import { EntityManager, getManager } from "typeorm";
import { Request, Response } from "express";

import { UpdateExistingUserRequest, DeleteUserRequest, CreateUserRequest, UpdateMultipleUsersRequest } from "../models";

export class UserController {

    public static async getAllUsers(_: Request, response: Response) {
        try {
            const users = await UserRepository.find({});
            sendResponse(response, 200, {users});
        } catch (error) {
            sendResponse(response, 400, {message: error.message});
        }
    }

    public static async updateExistingUser(request: Request, response: Response) {
        try {

            const body: UpdateExistingUserRequest = {...request.body};
            const userId = body.userId;

            const user = await UserRepository.findOne({where: {userId}});
            if(!user) throw new Error("User doesn't not exist");

            delete body.userId;
            await UserRepository.update({userId},{...body});

            sendResponse(response, 200, {});

        } catch (error) {
            sendResponse(response, 400, {message: error.message});
        }
    }

    public static async deleteUser(request: Request, response: Response) {
        try {

            const body: DeleteUserRequest = {...request.body};

            const user = await UserRepository.findOne({where: {userId: body.userId}});
            if(!user) throw new Error("User doesn't not exist");
            await UserRepository.delete({userId: body.userId});

            sendResponse(response, 200, {});
        } catch (error) {
            sendResponse(response, 400, {message: error.message});
        }
    }

    public static async createUser(request: Request, response: Response) {
        try {

            const user: CreateUserRequest = {...request.body};
            await UserRepository.insert(user);

            sendResponse(response, 200, {});

        } catch (error) {
            sendResponse(response, 400, {message: error.message});
        }
    }

    public static async updateMultipleUsers(request: Request, response: Response) {

        const body: UpdateMultipleUsersRequest = {...request.body};

            await getManager().transaction(async (entityManager: EntityManager) => {

                for await (const user of body.users) {

                    const userToBeUpdated = await UserRepository.findOne({where: {userId: user.userId}}, entityManager);
                    if(!user) throw new Error("User doest not exist");
        
                    delete user.userId;
                    await UserRepository.update({userId: userToBeUpdated.userId},{...user}, entityManager);
                }

            }).catch((error) => {
                sendResponse(response, 400, {message: error.message});
            })

            sendResponse(response, 200, {});
       
    }


}