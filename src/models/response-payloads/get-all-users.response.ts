import { User } from "../../entities/user.entity";

export class GetAllUsersResponse {
    users: User[];
}