import { UserStatus } from "../enums/user-status.enum";

export interface UpdateExistingUserRequest {
    userId: number;
    userStatus: UserStatus;
    name: string;
    dateOfBirth: Date;
}