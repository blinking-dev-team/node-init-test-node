import { User } from "../../entities/user.entity";

export interface UpdateMultipleUsersRequest {
    users: User[]
}