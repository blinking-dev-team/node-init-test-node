export enum UserStatus {
    Active = "Active",
    Locked = "Locked",
    Terminated = "Terminated"
}