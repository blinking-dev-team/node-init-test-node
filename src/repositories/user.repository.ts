import { DeepPartial, EntityManager, FindManyOptions, FindOneOptions, getManager } from "typeorm";
import { QueryDeepPartialEntity } from "typeorm/query-builder/QueryPartialEntity";
import { User } from "../entities/user.entity";

export class UserRepository {

    public static async insert(model: DeepPartial<User>, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).save(User, { ...model });
    }

    public static async findOne(options: FindOneOptions<User>, entityManager?: EntityManager): Promise<User | undefined> {
        return await (entityManager || getManager()).findOne(User, options);
    }

    public static async find(options: FindManyOptions<User>, entityManager?: EntityManager): Promise<User[]> {
        return await (entityManager || getManager()).find(User, options);
    }

    public static async update(criteria: any, partialEntity: QueryDeepPartialEntity<User>, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).update(User, criteria, partialEntity);
    }

    public static async delete(criteria: any, entityManager?: EntityManager): Promise<void> {
        await (entityManager || getManager()).delete(User, criteria);
    }
    
}