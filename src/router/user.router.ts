import { UserController } from "../api/user.controller";
import { Router } from "express";

const router = Router();

router.post('/create',UserController.createUser);
router.post('/delete',UserController.deleteUser);
router.post('/update', UserController.updateExistingUser);
router.post('/updateMany', UserController.updateMultipleUsers);
router.get('/', UserController.getAllUsers)

export const UserRouter = router;