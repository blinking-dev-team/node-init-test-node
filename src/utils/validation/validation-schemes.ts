import Joi from "joi";
import { UserStatus } from "../../models/enums/user-status.enum";

export const Schemes = {

    "/users/create": Joi.object({
        dateOfBirth: Joi.date().required(),
        name: Joi.date().required()
    }),

    "/users/update": Joi.object({
        userId: Joi.number().integer().min(0).required(),
        userStatus: Joi.string().valid(...Object.values(UserStatus)),
        dateOfBirth: Joi.date(),
        name: Joi.date()
    }),

    "/users/updateMany": Joi.object({
        users: Joi.array().items({
            userId: Joi.number().integer().min(0).required(),
            userStatus: Joi.string().valid(...Object.values(UserStatus)),
            dateOfBirth: Joi.date(),
            name: Joi.date()
        }).required()

    }),

    "/users/delete": Joi.object({
        userId: Joi.number().integer().min(0).required()
    }),

}
