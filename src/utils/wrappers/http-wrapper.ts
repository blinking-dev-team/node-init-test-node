
import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { readFileSync } from "fs";
import { Agent } from "https";

import { env } from "./env-wrapper";

export class HttpWrapper {

    private instance: AxiosInstance;

    public constructor(baseURL: string) {
        const httpsAgent = (baseURL.includes("https://")) ? new Agent(
            { pfx: readFileSync(env.cert.path), passphrase: env.cert.passphrase }
        ) : undefined;
        this.instance = axios.create({ baseURL, httpsAgent })
    }

    public async post(path: string, data: any, config?: AxiosRequestConfig): Promise<any> {
        const operation = this.instance.post.bind(this.instance, path, data, config);
        return await operation();
    }

    public async get(path: string, config?: AxiosRequestConfig): Promise<any> {
        const operation = this.instance.get.bind(this.instance, path, config);
        return await operation();
    }

}
