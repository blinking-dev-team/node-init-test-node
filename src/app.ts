import express from "express";
import { json } from "body-parser";

import * as routers from "./router/router"
import { validateRequestPayload } from "./utils/validation/validator";
import { sendInvalidMethodResponse } from "./utils/wrappers/response-wrapper";

const app: express.Application = express();

app.use(validateRequestPayload);

app.use(json({ limit: "50mb", type: "application/json" }));

app.use("/users",routers.UserRouter);

app.use(sendInvalidMethodResponse);

export default app;
